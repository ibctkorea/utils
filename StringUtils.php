<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 9. 27
 * Time: 오후 5:53
 */

namespace App\Utils;


class StringUtils
{
    public static function bcHexdec($hex)
    {
        $dec = 0;
        $len = strlen($hex);
        for ($i = 1; $i <= $len; $i++) {
            $dec = bcadd($dec, bcmul(strval(hexdec($hex[$i - 1])), bcpow('16', strval($len - $i))));
        }
        return $dec;
    }

    public static function bcDechex($dec) {
        $last = bcmod($dec, 16);
        $remain = bcdiv(bcsub($dec, $last), 16);

        if($remain == 0) {
            return dechex($last);
        } else {
            return self::bcDechex($remain).dechex($last);
        }
    }

    public static function rippleToUnixTimestamp($rippleTime)
    {
        return ($rippleTime + 946684800);
    }
}