<?php


namespace App\Utils;


class ToolsPrecision
{

    public static function precision($number, $precision){
        $number = stripos($number, 'e')!==false ? number_format($number, $precision+1, '.', '') : $number;
        $number = explode('.', $number);
        if(count($number)===1){
            $number[] = 0;
        }

        list($before, $after) = $number;
        return $precision>0 ? $before.'.'.substr(str_pad($after, $precision, '0', STR_PAD_RIGHT), 0, $precision) :  $before;
    }


}
