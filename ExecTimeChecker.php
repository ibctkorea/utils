<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 9. 27
 * Time: 오후 5:53
 */

namespace App\Utils;


class ExecTimeChecker
{
    private $checkList;

    public function __construct($msg = '') {
        $msg = empty($msg) ? 'Start' : $msg;
        $this->checkList = [
            ['msg' => $msg, 'time' => microtime(true)]
        ];
    }

    public function check($msg) {
        $this->checkList[] = ['msg' => $msg, 'time' => microtime(true)];
    }

    public function result() {
        $result = [];
        $startTime = $beforeTime = $this->checkList[0]['time'];

        foreach ($this->checkList as $val) {
            $result[] = $val['msg'] . ' : ' . ($val['time']) . '(' . (bcsub($val['time'], $beforeTime, 4)) . ')';
            $beforeTime = $val['time'];
        }

        if (count($result) > 2) {
            $result[] = 'Total Execute Time : (' . (bcsub($val['time'], $startTime, 4)) . ')';
        }

        return $result;
    }
}
